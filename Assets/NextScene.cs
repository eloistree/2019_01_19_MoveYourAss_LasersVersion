﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            int index = SceneManager.GetActiveScene().buildIndex + 1;
            if (index >= SceneManager.sceneCountInBuildSettings)
                index = SceneManager.sceneCountInBuildSettings - 1;
            SceneManager.LoadScene(index);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            int index = SceneManager.GetActiveScene().buildIndex - 1;
            if (index < 0)
                index = 0;
            SceneManager.LoadScene(index);
            
        }

    }
}
