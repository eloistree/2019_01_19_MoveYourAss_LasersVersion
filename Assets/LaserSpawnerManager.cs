﻿using EloiExperiments.Toolbox.RandomTool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSpawnerManager : MonoBehaviour
{
    public Transform m_direction;
    public GameObject[] m_laserPrefab;

    public float m_minTimeBetweenSpawn = 0.5f;
    public float m_maxTimeBetweenSpawn = 3f;
    public float m_horizontalRandom = 10;
    public float m_verticalRandom = 10;

    public float m_laserLifeTime=30f;

    private LasersInScene[] m_lasers;
    // Start is called before the first frame update
    IEnumerator Start()
    {

        m_lasers = LasersPool.GetLasers();
        while (true) {

            yield return new WaitForSeconds(RandomTool.GetFloat(m_minTimeBetweenSpawn, m_maxTimeBetweenSpawn));
            yield return new WaitForEndOfFrame();
            if (IsOneLasersAvailaible()) {
                Vector3 newPosition =  m_direction.position;
                newPosition += m_direction.right * RandomTool.GetFloat(m_horizontalRandom,true);
                newPosition += m_direction.up * RandomTool.GetFloat(m_verticalRandom, true);
                GameObject newLaser = GameObject.Instantiate( GetRandomLaserType(),newPosition, m_direction.rotation);
                LifeTime  time = newLaser.AddComponent<LifeTime>();
                time.SetTime(m_laserLifeTime);

            }
        }

        
    }
    public bool IsOneLasersAvailaible() {
        foreach (LasersInScene lasers in m_lasers)
        {
            if (!lasers.IsClaim())
                return true;
        }
        return false;
    }

    private GameObject GetRandomLaserType()
    {
        if(m_laserPrefab.Length==0)
            throw new System.IndexOutOfRangeException("This object must at least have one laser prefab");
        return m_laserPrefab[RandomTool.GetInteger(0, m_laserPrefab.Length - 1)];
    }
    
}
