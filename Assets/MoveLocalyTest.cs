﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLocalyTest : MonoBehaviour
{
    public Transform m_targetAffected;
    public float m_moveSpeed = 1f;
   
    void Update()
    {
        Vector3 move = Vector3.zero;
        if (Input.GetAxis("Vertical") != 0)
        {
            move.y = Time.deltaTime * m_moveSpeed;
        }
        if (Input.GetAxis("Horizontal") != 0)
        {
            move.z = Time.deltaTime * m_moveSpeed;
        }

        m_targetAffected.localPosition += move;

    }
}
