﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshToVoxel : MonoBehaviour
{
    public MeshFilter m_renderer;
    public Mesh m_mesh;
    public Dictionary<string, bool> m_voxel = new Dictionary<string, bool>();
    public int m_index;
    public int m_total;

    [Range(0,1f)]
    public float m_pourcent;
    public string m_id = "";
    public string m_xyz = "";
    public float m_multiple = 1000;
    public float m_divider = 10;
    public Vector3 m_min, m_max;
    public int m_numberOfCube;
    public int m_possibleCube;
    // Start is called before the first frame update
    public int m_antiLoop = 50;
    IEnumerator Start()
    {
        m_mesh = m_renderer.mesh;
        Vector3 p = Vector3.zero;
        m_total = m_mesh.vertices.Length;
        float m = m_multiple / m_divider;
        for (int i = 0; i < m_total; i++)
        {
            p = m_mesh.vertices[i]* m_multiple;
            if (p.x < m_min.x) m_min.x = (int) p.x;
            if (p.y < m_min.y) m_min.y = (int)p.y;
            if (p.z < m_min.z) m_min.z = (int)p.z;
            if (p.x > m_max.x) m_max.x = (int)p.x;
            if (p.y > m_max.y) m_max.y = (int)p.y;
            if (p.z > m_max.z) m_max.z = (int)p.z;
            m_possibleCube = (int)( (m_max.x - m_min.x) *
                            (m_max.y - m_min.y) *
                            (m_max.z - m_min.z));
                              
                              
                              


            m_xyz = string.Format("{0}:{1}:{2}",p.x, p.y, p.z);
            m_id = Index3.GetIdWith((int)p.x, (int)p.y, (int)p.z);
            if (!m_voxel.ContainsKey(m_id))
            {
                m_voxel.Add(m_id, true);
                Debug.DrawLine(new Vector3(0.5f+(int)p.x, (int)p.y, 0.5f + (int)p.z)* m, new Vector3(0.5f + (int)p.x, 1 + (int)p.y, 0.5f  + (int)p.z) * m, Color.green, 4000);
                m_numberOfCube ++;
            }
            m_index = i;
            m_pourcent = (float)m_index / (float)m_total;
            if (i% m_antiLoop == 0)
                yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();

        //int size = 10;
        //for (int x = 0; x < size; x++)
        //{
        //    for (int y = 0; y < size; y++)
        //    {
        //        for (int z = 0; z < size; z++)
        //        {
        //            m_mesh.vertices.Where(k=)
        //        }
        //    }
        //}




    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[System.Serializable]
public class Index3
{
    int x, y, z;
    public Index3(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static string GetIdWith(int x, int y, int z) { return string.Format("{0}:{1}:{2}", x, y, z); }
    public string GetId() { return string.Format("{0}:{1}:{2}", x, y, z); }
    public static Index3 GetIndexFrom(string id) {
        string[] tokens = id.Split(':');
        return new Index3(int.Parse(tokens[0]), int.Parse(tokens[1]), int.Parse(tokens[2]));
    }
}