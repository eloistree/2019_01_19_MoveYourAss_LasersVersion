﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickMoveFoward : MonoBehaviour
{
    public float m_speed= 5f;
    public Transform m_direction;
    public Transform m_affected;

    void Update()
    {
        m_affected.Translate(m_direction.forward * (m_speed*Time.deltaTime), Space.World);
    }
    private void Reset()
    {
        m_direction = transform;
        m_affected = transform;
    }
}
