﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTime : MonoBehaviour
{
    public float m_timeLeft=10;
    void Update()
    {
        m_timeLeft -= Time.deltaTime;
        if (m_timeLeft <= 0f)
            DestroyImmediate(gameObject);

    }

    internal void SetTime(float lifeTime)
    {
        m_timeLeft = lifeTime;
    }
}
