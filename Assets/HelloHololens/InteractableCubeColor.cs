﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;
using UnityEngine.UI;

public class InteractableCubeColor : MonoBehaviour, IFocusable, IInputHandler, ISpeechHandler
{

    public float m_scaleWhenFocus = 1.5f;
    [Header("Debug")]
    public Vector3 m_recordedScaleWhenFocus;
    public bool m_isFocus;
    public bool m_isDown;

    public Text m_displayRecognizeWord;

    public void OnFocusEnter()
    {
        m_isFocus = true;
        m_recordedScaleWhenFocus = transform.localScale;
        transform.localScale *= m_scaleWhenFocus;
    }

    public void OnFocusExit()
    {
        m_isFocus = false;
        transform.localScale = m_recordedScaleWhenFocus ;
    }

    void IInputHandler.OnInputDown(InputEventData eventData)
    {
        m_isDown = true;
        GetComponent<Renderer>().material.color = Color.white;
    }

    void IInputHandler.OnInputUp(InputEventData eventData)
    {

        m_isDown = false;
        GetComponent<Renderer>().material.color = GetRandomColor();
    }

    private Color GetRandomColor()
    {
        return new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }

    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        string text = eventData.RecognizedText;
        m_displayRecognizeWord.text = text;
        Color c = Color.white;
        switch (text.ToLower())
        {
            case "blue": c = Color.blue; break;
            case "red": c = Color.red; break;
            case "green": c = Color.green; break;
            case "white": c = Color.white; break;
            case "black": c = Color.black; break;
            case "transparent": c.a = 0.1f; break;
            case "opaque": c.a = 1f; break;
            default:
                break;
        }
        GetComponent<Renderer>().material.color = c;
    }
}
