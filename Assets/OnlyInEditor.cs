﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyInEditor : MonoBehaviour
{
    public bool m_destroyThisObject = true;
    public MonoBehaviour [] monoToRemove;
   void Awake()
    {

#if UNITY_EDITOR

#else
        foreach (MonoBehaviour mono in monoToRemove)
        {
            Destroy(mono);
        } 
        if(m_destroyThisObject)
            Destroy(this.gameObject);
#endif
    }
}
